package com.mycompany.l02;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class TestBase {

    WebDriver driver;
    WebDriverWait wait;

    @BeforeTest
    public void initData() {
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://intra.t-systems.ru/");
        wait = new WebDriverWait(driver, 10);
    }

    @AfterTest
    public void closeBrowser() {
        driver.quit();
    }


    public boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public void loginToIntra() throws IOException {
        ReadTestData testdata = new ReadTestData("testdata.properties");

        WebElement userName = driver.findElement(By.id("UserName"));
        userName.clear();
        userName.sendKeys(testdata.getUsername());

        WebElement password = driver.findElement(By.id("Password"));
        password.clear();
        password.sendKeys(testdata.getPassword());

        driver.findElement(By.id("logonButton")).click();
    }
}

