package com.mycompany.l02;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

import java.io.IOException;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class EditProfileTest extends TestBase {
    @Test
    public void editProfileTest() throws IOException {
        loginToIntra();

        WebElement userProfile = driver.findElement(By.xpath("//ul[@class='dropdown-menu']//a[contains(text(),'Мой профиль')]"));
        userProfile.click();
        assertTrue(driver.findElement(By.xpath("//div[@id='form_prof_dannie_head']")).isDisplayed());

        WebElement editName = wait.until(ExpectedConditions.elementToBeClickable(
                By.xpath("//a[@id='editDisplayName']//i[@class='icon-pencil']")));

        editName.click();

        WebElement editNameForm = driver.findElement(By.xpath("//form[@class='form-inline editableform']"));
        wait.until(ExpectedConditions.visibilityOf(editNameForm));

        WebElement firstNameEditForm = driver.findElement(By.xpath("//input[@name='firstName']"));
        firstNameEditForm.clear();
        firstNameEditForm.sendKeys("Mariia");

        WebElement secondNameEditForm = driver.findElement(By.xpath("//input[@name='lastName']"));
        firstNameEditForm.clear();
        firstNameEditForm.sendKeys("Naprasnikova");

        WebElement okButton = driver.findElement(By.xpath("//i[@class='icon-ok icon-white']"));
        okButton.click();
        assertTrue(!editNameForm.isDisplayed());

        WebElement nameSurname = driver.findElement(By.xpath("//h1[contains(@class,'editable')]"));
        assertEquals(nameSurname.getText(), "Mariia Naprasnikova");

    }
}
