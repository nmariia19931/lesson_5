package com.mycompany.l02;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ReadTestData {
    private String username;
    private String password;

    public String getUsername() {
        return username;
    }


    public String getPassword() {
        return password;
    }

    public ReadTestData(String propFileName) throws IOException {
        InputStream input = null;
        try {
            input = ReadTestData.class.getClassLoader().getResourceAsStream(propFileName);
            Properties prop = new Properties();
            //load a properties file from class path
            if (input != null) {
                prop.load(input);
            } else {
                throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
            }

            //get the property value
            username = prop.getProperty("username");
            password = prop.getProperty("password");
        } catch (Exception e) {
            System.out.println("Exception: " + e);
        } finally {
            if (input != null) {
                input.close();
            }
        }
    }
}
