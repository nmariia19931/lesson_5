package com.mycompany.l02;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

import java.io.IOException;

public class SearchGroup extends TestBase {
    @Test
    public void searchGroupTest() throws IOException {
        loginToIntra();
        WebElement groups = driver.findElement(By.xpath("//a[@href='/New/Groups']"));
        groups.click();

        WebElement findSearch = driver.findElement(By.xpath("//input[@placeholder='Введите название группы']"));
        wait.until(ExpectedConditions.elementToBeClickable(findSearch));
        findSearch.clear();
        findSearch.sendKeys("VRN Auto Test University");

        WebElement foundGroup = driver.findElement(By.xpath("//span[contains(text(),'VRN Auto Test Univer ...')]"));
        wait.until(ExpectedConditions.visibilityOf(foundGroup));
    }
}
